import org.gradle.internal.os.OperatingSystem

plugins {
  application
  id("org.jetbrains.kotlin.jvm").version("1.1.61")
}

group = "de.kuschku"
version = "1.0-SNAPSHOT"

configure<ApplicationPluginConvention> {
  mainClassName = "de.kuschku.atlas.MainKt"
}

repositories {
  jcenter()
  mavenCentral()
}

dependencies {
  compile(kotlin("stdlib"))

  compile("com.flowpowered", "flow-nbt", "1.0.0")

  val lwjglVersion = "3.1.5"
  val lwjglNatives = when {
    OperatingSystem.current().isWindows -> "natives-windows"
    OperatingSystem.current().isMacOsX -> "natives-macos"
    OperatingSystem.current().isLinux -> "natives-linux"
    else -> throw IllegalArgumentException("OS not supported")
  }
  compile("org.lwjgl:lwjgl:$lwjglVersion")
  compile("org.lwjgl:lwjgl-glfw:$lwjglVersion")
  compile("org.lwjgl:lwjgl-jemalloc:$lwjglVersion")
  compile("org.lwjgl:lwjgl-opengl:$lwjglVersion")
  compile("org.lwjgl:lwjgl-sse:$lwjglVersion")
  runtime("org.lwjgl:lwjgl:$lwjglVersion:$lwjglNatives")
  runtime("org.lwjgl:lwjgl-glfw:$lwjglVersion:$lwjglNatives")
  runtime("org.lwjgl:lwjgl-jemalloc:$lwjglVersion:$lwjglNatives")
  runtime("org.lwjgl:lwjgl-opengl:$lwjglVersion:$lwjglNatives")
  runtime("org.lwjgl:lwjgl-sse:$lwjglVersion:$lwjglNatives")
  compile("org.joml:joml:1.9.6")

  testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
  sourceCompatibility = JavaVersion.VERSION_1_8
}