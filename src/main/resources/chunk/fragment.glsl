#version 330 core

/*
in float blockLightLevel;
in float skyLightLevel;
*/
out vec4 FragColor;

void main()
{
    FragColor = vec4(0.5, 0.2, 0.1, 1.0);
}