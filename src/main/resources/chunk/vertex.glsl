#version 330 core
layout (location = 0) in vec3 aPos;
/*
layout (location = 1) in float aBlockLight;
layout (location = 2) in float aSkyLight;

out float blockLightLevel;
out float skyLightLevel;
*/

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
/*
    blockLightLevel = aBlockLight;
    skyLightLevel = aSkyLight;
    */
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}