package de.kuschku.atlas;

public class BrightnessGenerator {
    /**
     * Returns the value of the first parameter, clamped to be within the lower and upper limits given by the second and
     * third parameters
     */
    private static float clamp(float num, float min, float max)
    {
        if (num < min)
        {
            return min;
        }
        else
        {
            return num > max ? max : num;
        }
    }

    private float lightBrightnessTable(int i) {
        float f1 = 1.0F - (float)i / 15.0F;
        return (1.0F - f1) / (f1 * 3.0F + 1.0F) * 1.0F + 0.0F;
    }

    private float getCelestialAngle(float time) {
        float f = time - 0.25F;

        float f1 = 1.0F - (float)((Math.cos((double)f * Math.PI) + 1.0D) / 2.0D);
        f = f + (f1 - f) / 3.0F;
        return f;
    }

    private float sunBrightness(float time) {
        float f = getCelestialAngle(time);
        float f1 = (float) (1.0F - (Math.cos(f * ((float)Math.PI * 2F)) * 2.0F + 0.2F));
        f1 = clamp(f1, 0.0F, 1.0F);
        f1 = 1.0F - f1;
        return f1 * 0.8F + 0.2F;
    }

    public float[] determineBrightness(int y, int x, int mcTime, float gammaSetting, boolean nightVision, float nightVisionBrightness) {
        float sunLevel = sunBrightness(mcTime);
        float scaledSunBrightness = sunLevel * 0.95F + 0.05F;

        float skyLight = lightBrightnessTable(y) * scaledSunBrightness;
        float blockLight = lightBrightnessTable(x) * 1.5F;

        float skyLightBrightness = skyLight * (sunLevel * 0.65F + 0.35F);
        float blockLightGreen = blockLight * ((blockLight * 0.6F + 0.4F) * 0.6F + 0.4F);
        float blockLightBlue = (float) (blockLight * (Math.pow (blockLight, 2.0f) *0.6F + 0.4F));
        float redFloat = skyLightBrightness + blockLight;
        float greenFloat = skyLightBrightness + blockLightGreen;
        float blueFloat = skyLight + blockLightBlue;

        redFloat = redFloat * 0.96F + 0.03F;
        greenFloat = greenFloat * 0.96F + 0.03F;
        blueFloat = blueFloat * 0.96F + 0.03F;

        redFloat = clamp(redFloat, 0.0f, 1.0f);
        greenFloat = clamp(greenFloat, 0.0f, 1.0f);
        blueFloat = clamp(blueFloat, 0.0f, 1.0f);

        if (nightVision) {
            float nightVisionFactor = 1.0F / Math.max(redFloat, Math.max (greenFloat, blueFloat));

            redFloat = redFloat * (1.0F - nightVisionBrightness) + redFloat * nightVisionFactor * nightVisionBrightness;
            greenFloat =
                    greenFloat * (1.0F - nightVisionBrightness) + greenFloat * nightVisionFactor * nightVisionBrightness;
            blueFloat = blueFloat * (1.0F - nightVisionBrightness) + blueFloat * nightVisionFactor * nightVisionBrightness;
        }

        redFloat = clamp(redFloat, 0.0f, 1.0f);
        greenFloat = clamp(greenFloat, 0.0f, 1.0f);
        blueFloat = clamp(blueFloat, 0.0f, 1.0f);

        float redDiff = 1.0F - redFloat;
        float greenDiff = 1.0F - greenFloat;
        float blueDiff = 1.0F - blueFloat;

        redDiff = (float) (1.0F - Math.pow (redDiff, 4.0f));
        greenDiff = (float) (1.0F - Math.pow (greenDiff, 4.0f));
        blueDiff = (float) (1.0F - Math.pow (blueDiff, 4.0f));

        redFloat = redFloat * (1.0F - gammaSetting) + redDiff * gammaSetting;
        greenFloat = greenFloat * (1.0F - gammaSetting) + greenDiff * gammaSetting;
        blueFloat = blueFloat * (1.0F - gammaSetting) + blueDiff * gammaSetting;

        redFloat = redFloat * 0.96F + 0.03F;
        greenFloat = greenFloat * 0.96F + 0.03F;
        blueFloat = blueFloat * 0.96F + 0.03F;

        redFloat = clamp(redFloat, 0.0f, 1.0f);
        greenFloat = clamp(greenFloat, 0.0f, 1.0f);
        blueFloat = clamp(blueFloat, 0.0f, 1.0f);

        int red = (int) (redFloat * 255.0F);
        int green = (int) (greenFloat * 255.0F);
        int blue = (int) (blueFloat * 255.0F);

        return new float[]{red, green, blue};
    }
}
