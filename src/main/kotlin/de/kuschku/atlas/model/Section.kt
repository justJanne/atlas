package de.kuschku.atlas.model

data class Section(
  val y: Byte,
  val blockLight: ByteArray,
  val skyLight: ByteArray,
  val blocks: IntArray,
  val data: ByteArray
)