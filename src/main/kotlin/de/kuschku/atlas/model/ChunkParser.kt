package de.kuschku.atlas.model

import com.flowpowered.nbt.*
import com.flowpowered.nbt.stream.NBTInputStream
import de.kuschku.atlas.RegionFile
import de.kuschku.atlas.util.convertBlocks
import de.kuschku.atlas.util.convertNibbleArray
import java.io.File

fun parseSection(section: CompoundMap): Section {
  val blocksTag = (section["Blocks"] as ByteArrayTag).value
  val addTag = (section["Add"] as ByteArrayTag?)?.value
  val blocks = convertBlocks(blocksTag, addTag)
  return Section(
    y = (section["Y"] as ByteTag).value,
    blockLight = convertNibbleArray((section["BlockLight"] as ByteArrayTag).value),
    skyLight = convertNibbleArray((section["SkyLight"] as ByteArrayTag).value),
    blocks = blocks,
    data = convertNibbleArray((section["Data"] as ByteArrayTag).value)
  )
}

fun parseChunk(chunk: CompoundMap): Chunk {
  val level = (chunk["Level"] as CompoundTag).value
  val sections = (level["Sections"] as ListTag<CompoundTag>).value
  val heightMap = (level["HeightMap"] as IntArrayTag).value
  val xPos = (level["xPos"] as IntTag).value
  val zPos = (level["zPos"] as IntTag).value
  val builtSections = sections.map(CompoundTag::getValue).map(::parseSection)
  val sectionMap = mutableMapOf<Int, Section>()
  for (section in builtSections) {
    for (i in 0 until Chunk.CHUNK_SIZE) {
      sectionMap[section.y * Chunk.CHUNK_SIZE + i] = section
    }
  }
  return Chunk(
    position = Pair(xPos, zPos),
    heightmap = heightMap,
    sections = sectionMap
  )
}

fun parseRegionFile(pathName: String, chunkStorage: MutableList<Chunk>) {
  val regionFile = RegionFile(File(pathName))
  for ((x, z) in regionFile.chunks()) {
    val stream = NBTInputStream(regionFile.getChunkDataInputStream(x, z), false)
    chunkStorage.add(parseChunk((stream.readTag() as CompoundTag).value))
  }
}