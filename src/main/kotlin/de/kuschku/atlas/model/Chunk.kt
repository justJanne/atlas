package de.kuschku.atlas.model;

data class Chunk(val position: Pair<Int, Int>, val heightmap: IntArray, val sections: Map<Int, Section>) {
  companion object {
    val CHUNK_SIZE = 16
  }

  fun blockLight(x: Int, y: Int, z: Int): Byte {
    return sections[y]?.blockLight?.get((y % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + x) ?: 0
  }

  fun skyLight(x: Int, y: Int, z: Int): Byte {
    return sections[y]?.skyLight?.get((y % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + x) ?: 0
  }

  fun block(x: Int, y: Int, z: Int): Int {
    return sections[y]?.blocks?.get((y % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + x) ?: 0
  }

  fun data(x: Int, y: Int, z: Int): Byte {
    return sections[y]?.data?.get((y % CHUNK_SIZE) * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + x) ?: 0
  }
}
