package de.kuschku.atlas

import de.kuschku.atlas.mesh.Mesh
import de.kuschku.atlas.mesh.buildChunkMesh
import de.kuschku.atlas.model.Chunk
import de.kuschku.atlas.util.GLFWwindow
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWCursorPosCallback
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.glfw.GLFWFramebufferSizeCallback
import org.lwjgl.glfw.GLFWKeyCallback
import org.lwjgl.opengl.ARBDirectStateAccess.*
import org.lwjgl.opengl.ARBFramebufferObject.*
import org.lwjgl.opengl.GL.createCapabilities
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryUtil.NULL

class Demo {
  private var windowWidth: Int = 0
  private var windowHeight: Int = 0

  private var msaa: Boolean = true

  private var msaaFbo: Int = 0
  private var msaaColorBuffer: Int = 0
  private var msaaDepthBuffer: Int = 0
  private var samples: Int = 0

  private val view = Matrix4f()
  private val projection = Matrix4f()

  private var fps = 0
  private var lastTime = 0.0

  private var deltaTime = 0.0
  private var lastFrame = 0.0

  private var lastX = 0f
  private var lastY = 0f

  private var pitch = 0.0f
  private var yaw = -90.0f

  private var paused = true

  private val cameraPos = Vector3f(0f, 0f, 3f)
  private val cameraFront = Vector3f(0.0f, 0.0f, -1.0f)
  private val cameraUp = Vector3f(0.0f, 1.0f, 0.0f)

  fun demo(initialWidth: Int, initialHeight: Int, chunks: MutableList<Chunk>) {
    windowWidth = initialWidth
    windowHeight = initialHeight

    lastX = initialWidth / 2.0f
    lastY = initialHeight / 2.0f

    glfwSetErrorCallback(errorCallback)

    if (!glfwInit()) {
      throw IllegalStateException("Unable to initialize GLFW")
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)

    val window = glfwCreateWindow(windowWidth, windowHeight, "Example", NULL, NULL)
    if (window == NULL) {
      glfwTerminate()
      throw IllegalStateException("Unable to create GLFW window")
    }

    glfwMakeContextCurrent(window)

    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback)

    glfwSetKeyCallback(window, keyCallback)

    createCapabilities()

    msaaFbo = glCreateFramebuffers()
    msaaColorBuffer = glCreateRenderbuffers()
    msaaDepthBuffer = glCreateRenderbuffers()

    samples = glGetInteger(GL_MAX_SAMPLES)

    framebufferSizeCallback(window, windowWidth, windowHeight)

    glfwSetCursorPosCallback(window, cursorPosCallback)

    val chunkShaderProgram = ShaderProgram(
      this::class.java.getResource("/chunk/vertex.glsl").readText(),
      this::class.java.getResource("/chunk/fragment.glsl").readText()
    )

    val chunkMeshes = chunks.map { buildChunkMesh(chunkShaderProgram, it) }
    chunkMeshes.forEach(Mesh::create)

    glEnable(GL_DEPTH_TEST)

    while (!glfwWindowShouldClose(window)) {
      // input
      // -----
      processInput(window)

      if (msaa) {
        glBindFramebuffer(GL_FRAMEBUFFER, msaaFbo)
        glNamedFramebufferRenderbuffer(msaaFbo, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, msaaColorBuffer)
        glNamedFramebufferRenderbuffer(msaaFbo, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, msaaDepthBuffer)
        glNamedFramebufferRenderbuffer(msaaFbo, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, msaaDepthBuffer)
      }

      // render
      // ------
      glClearColor(0.2f, 0.3f, 0.3f, 1.0f)
      glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

      cameraFront.set(
        (Math.cos(Math.toRadians(pitch.toDouble())) * Math.cos(Math.toRadians(yaw.toDouble()))).toFloat(),
        (Math.sin(Math.toRadians(pitch.toDouble()))).toFloat(),
        (Math.cos(Math.toRadians(pitch.toDouble())) * Math.sin(Math.toRadians(yaw.toDouble()))).toFloat()
      ).normalize()

      view.setLookAt(
        cameraPos,
        Vector3f(cameraPos).add(cameraFront),
        cameraUp
      )

      for (chunkMesh in chunkMeshes) {
        chunkMesh.draw(view, projection)
      }

      if (msaa) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
        glBindFramebuffer(GL_READ_FRAMEBUFFER, msaaFbo)
        glDrawBuffer(GL_BACK)
        glBlitFramebuffer(0, 0, windowWidth, windowHeight, 0, 0, windowWidth, windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST)
      }

      // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
      // -------------------------------------------------------------------------------
      glfwSwapBuffers(window)
      glfwPollEvents()

      determineFPS()
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    chunkMeshes.forEach(Mesh::free)

    glfwDestroyWindow(window)
    framebufferSizeCallback.free()

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate()
    errorCallback.free()
  }

  private fun determineFPS() {
    fps++
    val time = glfwGetTime()
    deltaTime = time - lastFrame
    lastFrame = time
    val elapsedSeconds = Math.floor(time) - Math.floor(lastTime)
    if (elapsedSeconds > 1) {
      lastTime = Math.floor(time)
      println("$fps ${1000.0 / fps}ms")
      fps = 0
    }
  }

  private fun processInput(window: GLFWwindow) {
    val cameraSpeed = 2.5f * deltaTime.toFloat()
    val yBefore = cameraPos.y
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
      cameraPos.add(Vector3f(cameraFront).mul(cameraSpeed))
      cameraPos.y = yBefore
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
      cameraPos.sub(Vector3f(cameraFront).mul(cameraSpeed))
      cameraPos.y = yBefore
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
      cameraPos.sub(Vector3f(cameraFront).cross(cameraUp).normalize().mul(cameraSpeed))
      cameraPos.y = yBefore
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
      cameraPos.add(Vector3f(cameraFront).cross(cameraUp).normalize().mul(cameraSpeed))
      cameraPos.y = yBefore
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
      cameraPos.add(Vector3f(cameraUp).mul(cameraSpeed))
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) {
      cameraPos.sub(Vector3f(cameraUp).mul(cameraSpeed))
    }
  }

  private val keyCallback = GLFWKeyCallback.create { window, key, scancode, action, mods ->
    if (action == GLFW_RELEASE) when (key) {
      GLFW_KEY_ESCAPE -> glfwSetWindowShouldClose(window, true)
      GLFW_KEY_M -> msaa = !msaa
      GLFW_KEY_P -> {
        val cursor_x = windowWidth / 2.0
        val cursor_y = windowHeight / 2.0
        glfwSetCursorPos(window, cursor_x, cursor_y)
        lastX = cursor_x.toFloat()
        lastY = cursor_y.toFloat()
        glfwSetInputMode(window, GLFW_CURSOR, if (paused) GLFW_CURSOR_DISABLED else GLFW_CURSOR_NORMAL)
        paused = !paused
      }
    }
  }

  private val framebufferSizeCallback = GLFWFramebufferSizeCallback.create { _, width, height ->
    this.windowWidth = width
    this.windowHeight = height

    projection.identity().perspective(Math.toRadians(45.0).toFloat(), width * 1.0f / height, 0.1f, 100.0f)

    glViewport(0, 0, width, height)

    glNamedRenderbufferStorageMultisample(msaaColorBuffer, samples, GL_RGBA8, width, height)
    glNamedRenderbufferStorageMultisample(msaaDepthBuffer, samples, GL_DEPTH24_STENCIL8, width, height)
    glNamedFramebufferRenderbuffer(msaaFbo, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, msaaColorBuffer)
    glNamedFramebufferRenderbuffer(msaaFbo, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, msaaDepthBuffer)
    glNamedFramebufferRenderbuffer(msaaFbo, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, msaaDepthBuffer)
  }

  private val cursorPosCallback = GLFWCursorPosCallback.create { _, xpos, ypos ->
    if (!paused) {
      var xoffset: Float = xpos.toFloat() - lastX
      var yoffset: Float = lastY - ypos.toFloat()

      lastX = xpos.toFloat()
      lastY = ypos.toFloat()

      val sensitivity = 0.05f
      xoffset *= sensitivity
      yoffset *= sensitivity

      yaw += xoffset
      pitch += yoffset

      if (pitch > 89.0f)
        pitch = 89.0f

      if (pitch < -89.0f)
        pitch = -89.0f
    }
  }

  private val errorCallback = GLFWErrorCallback.createThrow()
}