package de.kuschku.atlas.util

import kotlin.experimental.and

fun convertNibbleArray(array: ByteArray): ByteArray {
  val result = ByteArray(array.size * 2)
  for ((i, value) in array.withIndex()) {
    val loNibble = value and 0x0F
    val hiNibble = ((value.toInt() and 0xFF) shr 4 and 0x0F).toByte()
    result[i * 2] = loNibble
    result[i * 2 + 1] = hiNibble
  }
  return result
}

fun convertBlocks(blocks: ByteArray, add: ByteArray?): IntArray {
  val result = IntArray(blocks.size)
  if (add != null) {
    for ((i, value) in add.withIndex()) {
      val loNibble: Int = (value and 0x0F).toInt()
      val hiNibble: Int = ((value.toInt() and 0xFF) shr 4 and 0x0F)

      result[i * 2] = ((loNibble shl 8) + blocks[i * 2])
      result[i * 2 + 1] = hiNibble shl 8 + blocks[i * 2 + 1]
    }
  } else {
    for ((i, value) in blocks.withIndex()) {
      result[i] = value.toInt() and 0xFF
    }
  }
  return result
}