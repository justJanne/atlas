package de.kuschku.atlas.util

import org.lwjgl.system.MemoryStack

typealias GLFWwindow = Long

inline fun <reified T : Any> sizeof(): Int {
  return when (T::class) {
    Float::class, Float::class.javaObjectType, Float::class.javaPrimitiveType -> 4
    Int::class, Int::class.javaObjectType, Int::class.javaPrimitiveType -> 4
    Long::class, Long::class.javaObjectType, Long::class.javaPrimitiveType -> 8
    Short::class, Short::class.javaObjectType, Short::class.javaPrimitiveType -> 2
    Byte::class, Byte::class.javaObjectType, Byte::class.javaPrimitiveType -> 1
    else -> throw IllegalArgumentException("Unsupported type: ${T::class}")
  }
}

inline fun memoryStack(block: MemoryStack.() -> Unit) {
  val stack = MemoryStack.stackPush()
  var closed = false
  try {
    return block(stack)
  } catch (e: Exception) {
    closed = true
    try {
      stack?.close()
    } catch (closeException: Exception) {
    }
    throw e
  } finally {
    if (!closed) {
      stack?.close()
    }
  }
}