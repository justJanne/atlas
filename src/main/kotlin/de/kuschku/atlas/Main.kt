package de.kuschku.atlas

import de.kuschku.atlas.model.Chunk
import de.kuschku.atlas.model.parseRegionFile

fun main(args: Array<String>) {
  val width = 800
  val height = 600

  val chunks = mutableListOf<Chunk>()
  parseRegionFile("data/r.0.0.mca", chunks)

  val demo = Demo()
  demo.demo(width, height, chunks)
}