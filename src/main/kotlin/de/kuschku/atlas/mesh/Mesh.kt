package de.kuschku.atlas.mesh

import de.kuschku.atlas.ShaderProgram
import de.kuschku.atlas.util.memoryStack
import de.kuschku.atlas.util.sizeof
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.opengl.ARBDirectStateAccess.*
import org.lwjgl.opengl.ARBVertexArrayObject.glBindVertexArray
import org.lwjgl.opengl.ARBVertexArrayObject.glDeleteVertexArrays
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL15.GL_STATIC_DRAW
import org.lwjgl.opengl.GL15.glDeleteBuffers
import org.lwjgl.opengl.GL20.glUniformMatrix4fv
import org.lwjgl.system.MemoryUtil.NULL

open class Mesh(val shader: ShaderProgram, protected val vertices: FloatArray) {
  protected var vao: Int = -1
  protected var vbo: Int = -1

  protected val model = Matrix4f()

  var position = Vector3f(0.0f, 0.0f, 0.0f)
  var scale = 1.0f

  protected var strides = mutableListOf(
    Stride(
      typeSize = sizeof<Float>(),
      glType = GL_FLOAT,
      size = 3
    )
  )

  open fun create() {
    vao = glCreateVertexArrays()
    vbo = glCreateBuffers()

    glNamedBufferData(vbo, vertices, GL_STATIC_DRAW)

    val strideWidth = strides.sumBy { it.typeSize * it.size }
    var offset = 0
    for ((index, stride) in strides.withIndex()) {
      glEnableVertexArrayAttrib(vao, index)
      glVertexArrayVertexBuffer(vao, 0, vbo, NULL + offset, strideWidth)
      glVertexArrayAttribFormat(vao, index, stride.size, stride.glType, false, 0)
      glVertexArrayAttribBinding(vao, index, 0)
      offset += stride.typeSize + stride.size
    }
  }

  protected fun updateUniforms(view: Matrix4f, projection: Matrix4f) {
    memoryStack {
      val floatBuffer = mallocFloat(16)

      view.get(floatBuffer)
      glUniformMatrix4fv(shader["view"], false, floatBuffer)

      projection.get(floatBuffer)
      glUniformMatrix4fv(shader["projection"], false, floatBuffer)

      model.identity()
        .translate(position)
        .scale(scale)
        .get(floatBuffer)
      glUniformMatrix4fv(shader["model"], false, floatBuffer)
    }
  }

  open fun draw(view: Matrix4f, projection: Matrix4f) {
    shader.use()
    updateUniforms(view, projection)

    glBindVertexArray(vao)
    glDrawArrays(GL_TRIANGLES, vertices.size, GL_FLOAT)
    glBindVertexArray(0)
  }

  open fun free() {
    if (vao != -1) glDeleteVertexArrays(vao)
    if (vbo != -1) glDeleteBuffers(vbo)
  }

  data class Stride(
    val typeSize: Int,
    val glType: Int,
    val size: Int
  )
}

