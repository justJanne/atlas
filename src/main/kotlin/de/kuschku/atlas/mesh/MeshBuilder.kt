package de.kuschku.atlas.mesh

import de.kuschku.atlas.ShaderProgram
import de.kuschku.atlas.model.Chunk
import org.joml.Vector3f

val cubeVertices = arrayOf(
/* 0 */ Vector3f(+0.5f, +0.5f, -0.5f),  // back top right
/* 1 */ Vector3f(+0.5f, -0.5f, -0.5f),  // back bottom right
/* 2 */ Vector3f(-0.5f, -0.5f, -0.5f),  // back bottom left
/* 3 */ Vector3f(-0.5f, +0.5f, -0.5f),  // back top left
/* 4 */ Vector3f(+0.5f, +0.5f, +0.5f),  // front top right
/* 5 */ Vector3f(+0.5f, -0.5f, +0.5f),  // front bottom right
/* 6 */ Vector3f(-0.5f, -0.5f, +0.5f),  // front bottom left
/* 7 */ Vector3f(-0.5f, +0.5f, +0.5f)   // front top left
)

val cubeIndices = intArrayOf(  // note that we start from 0!
  2, 1, 0,
  0, 3, 2,

  6, 5, 4,
  4, 7, 6,

  7, 3, 2,
  2, 6, 7,

  4, 0, 1,
  1, 5, 4,

  2, 1, 5,
  5, 6, 2,

  3, 0, 4,
  4, 7, 3
)

fun buildChunkMesh(shaderProgram: ShaderProgram, chunk: Chunk): ChunkMesh {
  val zero = 0.toByte()

  val vertices = ArrayList<Float>()
  val indices = ArrayList<Int>()
  var cubeIndex = 0
  val chunkPosition = Vector3f(chunk.position.first.toFloat(), 0f, chunk.position.second.toFloat()).mul(Chunk.CHUNK_SIZE.toFloat())

  for (section in chunk.sections.values) {
    val sectionPosition = Vector3f(0f, section.y * Chunk.CHUNK_SIZE.toFloat(), 0f).add(chunkPosition)
    for (y in 0 until Chunk.CHUNK_SIZE) {
      for (z in 0 until Chunk.CHUNK_SIZE) {
        for (x in 0 until Chunk.CHUNK_SIZE) {
          val blockIndex = y * Chunk.CHUNK_SIZE * Chunk.CHUNK_SIZE + z * Chunk.CHUNK_SIZE + x

          val position = Vector3f(x.toFloat(), y.toFloat(), z.toFloat()).add(sectionPosition)
          if (section.blocks[blockIndex] != 0) {
            val skyLight = chunk.skyLight(x, section.y * Chunk.CHUNK_SIZE + y + 1, z)
            if (skyLight != zero) {
              val tmp = Vector3f()

              for (index in cubeIndices) {
                indices.add(index + cubeVertices.size * cubeIndex)
              }

              for (vertex in cubeVertices) {
                tmp.set(position).add(vertex).div(64f)
                vertices.add(tmp.x)
                vertices.add(tmp.y)
                vertices.add(tmp.z)
                /*
                vertices[verticesIndex++] = blockLight.toFloat()
                vertices[verticesIndex++] = skyLight.toFloat()
                */
              }

              cubeIndex++
            }
          }
        }
      }
    }
  }

  return ChunkMesh(shaderProgram, vertices.toFloatArray(), indices.toIntArray())
}