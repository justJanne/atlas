package de.kuschku.atlas.mesh

import de.kuschku.atlas.ShaderProgram
import org.joml.Matrix4f
import org.lwjgl.opengl.ARBDirectStateAccess
import org.lwjgl.opengl.ARBVertexArrayObject
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL15

open class IndexedMesh(program: ShaderProgram, vertices: FloatArray, protected val indices: IntArray) : Mesh(program, vertices) {
  protected var ebo: Int = -1

  override fun create() {
    super.create()

    ebo = ARBDirectStateAccess.glCreateBuffers()
    ARBDirectStateAccess.glNamedBufferData(ebo, indices, GL15.GL_STATIC_DRAW)
    ARBDirectStateAccess.glVertexArrayElementBuffer(vao, ebo)
  }

  override fun draw(view: Matrix4f, projection: Matrix4f) {
    shader.use()
    updateUniforms(view, projection)

    ARBVertexArrayObject.glBindVertexArray(vao)
    GL11.glDrawElements(GL11.GL_TRIANGLES, indices.size, GL11.GL_UNSIGNED_INT, 0)
    ARBVertexArrayObject.glBindVertexArray(0)
  }

  override fun free() {
    if (ebo != -1) GL15.glDeleteBuffers(ebo)
  }
}