package de.kuschku.atlas.mesh

import de.kuschku.atlas.ShaderProgram

class ChunkMesh(program: ShaderProgram, vertices: FloatArray, indices: IntArray) : IndexedMesh(program, vertices, indices) {
  init {
    /*
    // Block Light
    strides.add(Stride(
      sizeof<Int>(),
      GL_INT,
      1
    ))
    // Sky Light
    strides.add(Stride(
      sizeof<Int>(),
      GL_INT,
      1
    ))
    */
  }
}