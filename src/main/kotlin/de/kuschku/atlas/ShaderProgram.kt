package de.kuschku.atlas

import org.lwjgl.opengl.GL20.*

class ShaderProgram private constructor() {
  private var id: Int = -1

  constructor(vertexShaderSource: String, fragmentShaderSource: String) : this() {
    val vertexShader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertexShader, vertexShaderSource)
    glCompileShader(vertexShader)
    if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) == 0) {
      println("ERROR::SHADER::VERTEX::COMPILATION_FAILED")
      println(glGetShaderInfoLog(vertexShader))
    }

    val fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragmentShader, fragmentShaderSource)
    glCompileShader(fragmentShader)
    if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == 0) {
      println("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED")
      println(glGetShaderInfoLog(fragmentShader))
    }

    id = glCreateProgram()
    glAttachShader(id, vertexShader)
    glAttachShader(id, fragmentShader)
    glLinkProgram(id)
    if (glGetProgrami(id, GL_LINK_STATUS) == 0) {
      println("ERROR::SHADER::PROGRAM::LINKING_FAILED")
      println(glGetProgramInfoLog(id))
    }
    glDeleteShader(fragmentShader)
    glDeleteShader(vertexShader)
  }

  operator fun get(name: CharSequence) = glGetUniformLocation(id, name)
  fun uniformLocation(name: CharSequence) = glGetUniformLocation(id, name)

  operator fun set(name: CharSequence, value: Boolean) = glUniform1i(uniformLocation(name), if (value) 1 else 0)
  operator fun set(name: CharSequence, value: Int) = glUniform1i(uniformLocation(name), value)
  operator fun set(name: CharSequence, value: Float) = glUniform1f(uniformLocation(name), value)

  fun use() = glUseProgram(id)
}